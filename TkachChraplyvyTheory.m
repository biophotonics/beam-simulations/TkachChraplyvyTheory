clear

%% Constants
c = 3e8; % [m/s] Speed of light in vacuum

%% Input parameters
% lambda = 1064e-9; % [m] Laser free-running wavelength
alpha = 6; % [-] Linewidth enhancement factor

L_chip = 5e-3; % [m] Chip resonator length
n_chip = 3; % [-] Chip refractive index

L_external = 15e-2; % [m] Distance from front facet to external reflector

R_s = 5e-3; % [-] Front facet reflectivity
% R_e = 15e-7; % [-] Mode-matched external reflection
% R_e = 10e-5;
R_e = 5e-7;

%% Additional parameter initializations
% omega_0 = 2*pi*c/lambda; % [Hz] Laser free-running angular frequency

tau_s = 2*L_chip*n_chip/c; % [s] Chip round trip time
tau_e = 2*L_external/c; % [s] External cavity round trip delay time for undisplaced external reflector

kappa = 1/tau_s*(1-R_s)/sqrt(R_s)*sqrt(R_e); % [Hz] "Feedback strength"
kappa_tau_e = kappa*tau_e; % [-]

%% Equation solving

tic

omega_0_tau_e_mod2pi_vec = linspace(0,10*pi,1000); % [-] Array of phase shift values to loop over

% Equation (3) states omega - omega_0 + kappa*(sin(omega*tau_e) + alpha*cos(omega*tau_e)) = 0
Deltaomega_tau_e_trial_vec = linspace(-4*pi,4*pi,1000); % [-] Array of different trial values of Deltaomega*tau_e

eq3_mat = Deltaomega_tau_e_trial_vec' + kappa_tau_e*(sin(Deltaomega_tau_e_trial_vec' + omega_0_tau_e_mod2pi_vec) + alpha*cos(Deltaomega_tau_e_trial_vec' + omega_0_tau_e_mod2pi_vec));
contourline = contourc(omega_0_tau_e_mod2pi_vec,Deltaomega_tau_e_trial_vec,eq3_mat,[0 0]); % Eq 3 is solved by finding the contour at 0 in a 2D matrix
omega_0_tau_e_mod2pi_values = NaN(1,size(contourline,2)-1); % NaNs are used to separate un-connected contour segments
Deltaomega_tau_e_values = NaN(1,size(contourline,2)-1); % NaNs are used to separate un-connected contour segments
index = 1;
while index < size(contourline,2)
    omega_0_tau_e_mod2pi_values(index:(index + contourline(2,index)) - 1) = contourline(1,(index+1):(index + contourline(2,index)));
    Deltaomega_tau_e_values(index:(index + contourline(2,index)) - 1) = contourline(2,(index+1):(index+contourline(2,index)));
    index = index + contourline(2,index) + 1;
end

toc

%% Data plotting

figure(1);clf;
plot(omega_0_tau_e_mod2pi_values/pi , Deltaomega_tau_e_values/pi,'k');
xlabel('\omega_0\tau_e mod 2\pi [\pi]');
ylabel('\Delta\omega\tau_e [\pi]');
xlim([0,10]);
ylim([-4,4]);
title(['\kappa\tau_e = ' num2str(kappa_tau_e)]);
grid on;
grid minor;
saveas(1,'Frequency.png');

DeltaG_tau_e_values = -2*kappa_tau_e*cos(Deltaomega_tau_e_values + omega_0_tau_e_mod2pi_values);
figure(2);clf;
plot(omega_0_tau_e_mod2pi_values/pi , DeltaG_tau_e_values,'k');
xlabel('\omega_0\tau_e mod 2\pi [\pi]');
ylabel('\DeltaG\tau_e [-]');
xlim([0,10]);
ylim([-4,4]);
title(['\kappa\tau_e = ' num2str(kappa_tau_e)]);
grid on;
grid minor;
saveas(2,'Gain.png');

delta_f_over_delta_f_0 = 1./(1+sqrt(1+alpha^2)*kappa_tau_e*cos(Deltaomega_tau_e_values + omega_0_tau_e_mod2pi_values + atan(alpha))).^2;
figure(3);clf;
plot(omega_0_tau_e_mod2pi_values/pi , 10*log10(delta_f_over_delta_f_0),'k');
xlabel('\omega_0\tau_e mod 2\pi [\pi]');
ylabel('\deltaf/\deltaf_0 [dB]');
xlim([0,10]);
ylim([-20,20]);
title(['\kappa\tau_e = ' num2str(kappa_tau_e)]);
grid on;
grid minor;
saveas(3,'Linewidth.png');

